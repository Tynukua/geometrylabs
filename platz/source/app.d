import std.stdio;
import window: Window;
import gobject: GraphicsObject;
import bindbc.opengl;

void main()
{
    Window w = new Window(900,900,"Platz");
    auto o = new GraphicsObject(
            // COORDS RANdoms
            [0,0,0,  //VERTEX
            0,1,0,
            1,0,0,
            1,1,0,
            0,0,1,  
            0,1,1,
            1,0,1,
            1,1,1],
            [0,0,1, // NORMALS
            0,0,-1,
            0,1,0,
            0,-1,0,
            1,0,0,
            -1,0,0
            ],
            [1,1,1,
            2,1,1,
            3,1,1,
            3,1,1,
            2,1,1,
            4,1,1]
            );
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS); 
    while(!w.shouldClose){
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        glClearColor(0.2f, 0.2f, 0.4f, 0.0f);
        o.enable();
        o.loadData();
        o.draw();
        o.disable();

        w.swapBuffers();
        w.pollEvents();
          
    }
    
}

import gl3n.linalg:mat4, vec4, vec3;
import std.stdio;
import shader:ShaderProgram;
import bindbc.opengl;

class GraphicsObject{
    float[] vertexBuffer;
    GLuint vertexBufferID;

    float[] normalBuffer;
    GLuint normalBufferID;

    float[] uvBuffer;
    GLuint uvBufferID;

    uint[] indexBuffer;
    GLuint indexBufferID;
    GLuint VertexArrayID;

    GLuint MatrixID;
    mat4 MVP;
    ShaderProgram program ;
    GLuint lightPosID; 
    this(
            float[] vertexs, 
            float[] normals, 
            uint[] indicies,
            string textureFilePath = null){
        uvBuffer = [0.9,0.9];
        vertexBuffer = vertexs;
        normalBuffer = normals;
        indexBuffer = indicies;
        program = ShaderProgram( 
           [
            "vertex_shader.glsl": GL_VERTEX_SHADER, 
            "fragment_shader.glsl": GL_FRAGMENT_SHADER] );
        mat4 projection = mat4.perspective(900,900,45,0.1f,100.0f);
        mat4 view= mat4.look_at(
           vec3(4, 2, 4),
           vec3(0, 0, 0),
           vec3(0, 1, 0));
        mat4 model = mat4(
           vec4(1,0,0,0),
           vec4(0,1,0,0),
           vec4(0,0,1,0),
           vec4(0,0,0,1),
           );
        lightPosID = glGetUniformLocation(program.programID, "lightPos");
        glUniform3f(lightPosID, 1,1,1);
        MatrixID = glGetUniformLocation(program.programID, "MVP");
        MVP = projection * view  * model;
	    glGenVertexArrays(1, &VertexArrayID);
        glBindVertexArray(VertexArrayID);
        glGenBuffers(1, &vertexBufferID);
        glGenBuffers(1, &uvBufferID);
        glGenBuffers(1, &normalBufferID);
        glGenBuffers(1, &indexBufferID);
    }
    void loadData(){
        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
        glNamedBufferData(
            vertexBufferID,
            vertexBuffer.length * float.sizeof,
            vertexBuffer.ptr,
            GL_STATIC_DRAW);

        glNamedBufferData(
            normalBufferID,
            normalBuffer.length * float.sizeof,
            normalBuffer.ptr,
            GL_STATIC_DRAW);


        glNamedBufferData(
            uvBufferID,
            indexBuffer.length * float.sizeof,
            indexBuffer.ptr,
            GL_STATIC_DRAW);
	    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferID);
        glBufferData(
            GL_ELEMENT_ARRAY_BUFFER,
            indexBuffer.length * uint.sizeof,
            indexBuffer.ptr,
            GL_STATIC_DRAW);
    }
    void enable(){	
        glEnableVertexAttribArray(0);
    	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);
    	glVertexAttribPointer(
    		0,                  // attribute
    		3,                  // size
    		GL_FLOAT,           // type
    		GL_FALSE,           // normalized?
    		0,                  // stride
    		null            // array buffer offset
    	);

    	// 2nd attribute buffer : UVs
    	glEnableVertexAttribArray(1);
    	glBindBuffer(GL_ARRAY_BUFFER, uvBufferID);
    	glVertexAttribPointer(
    		1,                                // attribute
    		2,                                // size
    		GL_FLOAT,                         // type
    		GL_FALSE,                         // normalized?
    		0,                                // stride
    		null                          // array buffer offset
    	);
    
        glEnableVertexAttribArray(2);
    	glBindBuffer(GL_ARRAY_BUFFER, normalBufferID);
    	glVertexAttribPointer(
    		2,                                // attribute
    		3,                                // size
    		GL_FLOAT,                         // type
    		GL_FALSE,                         // normalized?
    		0,                                // stride
    		null                              // array buffer offset
    	);
    }
    void draw(){
        program.use();
        glBindVertexArray(VertexArrayID);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferID);
    
        glDrawElements(GL_TRIANGLES,
            cast(int)indexBuffer.length,
            GL_UNSIGNED_INT,
            null);
        glBindVertexArray(0);
    }

    void disable(){
        static foreach(int i; 0..3)
            glDisableVertexAttribArray(i);
    }



}

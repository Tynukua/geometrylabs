import std.conv:to;
import std.stdio;

import derelict.opengl3.gl3;

float[] vecMul(float[]a, float[]b){
    return [
        a[1]*b[2] - a[2]*b[1],
        a[2]*b[0] - a[0]*b[2],
        a[0]*b[1] - a[1]*b[0]
    ];
}

float[] vecMul(float[] e, float[]a, float[] b){
    return vecMul([a[0]-e[0],a[1]-e[1],a[2]-e[2]],
            [b[0]-e[0],b[1]-e[1],b[2]-e[2]]);
    
}

float[] shell(float[] points){
    import core.exception;
    import std.stdio;
    ConvexHull h = new ConvexHull(points);
    float[] result;
    foreach(Flatness flat;h.verge){
        result~= h.points[flat.first*3..flat.first*3+3];
        result~= h.points[flat.second*3..flat.second*3+3];
        result~= h.points[flat.third*3..flat.third*3+3];
    }
    return result;

}

float angle(float[] a, float[]b){
    import std.math: acos;
    auto la = len(a), lb = len(b);
    auto prod = a[0]*b[0] +a[1]*b[1] +a[2]*b[2];
    if (prod ==0) return 0;
    return acos(prod/(la*lb));


}
float len(float[] v){
    import std.math:sqrt;
    float sum = 0;
    foreach(float _;v){
        sum+=_*_;
    }
    return sqrt(sum);
}

struct Edge
{
    int first;
    int second;
    int flatness; 
    bool is_close = false; 
    this(int first, int second, int flatness = -1, float[] normal = [0,0,0]) {
        this.first=first;
        this.second = second;
        this.flatness = flatness;
    }
}
struct Flatness 
{
    int first;
    int second;
    int third;
    float[] normal;

    this(int first, int second, int third, float[] normal) {
        this.first=first;
        this.second = second;
        this.third = third;
        this.normal = normal;
    }
    int another(int one, int two){
    return 0;}
}

class ConvexHull
{

    float[] points = []; 
    Flatness[] verge = [];  
    Edge[] edges = []; // Ребра

    int count_; 

    int findMinZ() const{
        int min_id = 0;
        for (int i = 1; i < count_; ++i)
        {
            if(points[i*3+1] <points[min_id*3+1])
                min_id = i;
            else if(points[i*3+1] == points[min_id*3+1]){
                if (points[i*3]  <points[min_id*3] || 
                        points[i*3+2] < points[min_id*3+2])
                    min_id = i;
            }
        }

        return min_id;
    }
    void findFirstFlatness(){
        int first_point, second_point, third_point; 
        first_point = findMinZ();
        double min_angle = 7;
        int min_id = -1;
        for (int i = 0; i < count_; ++i)
        {
            if (first_point == i) 
            {
                continue;
            }
            float[] start = points[first_point*3..first_point*3+3];
            float[] next = points[i*3..i*3+3];
            double angle = angle(
                    [start[0]- next[0],start[1]- next[1],start[2]- next[2]],
                    [0,next[1]-start[1],0]);
            if (min_angle > angle)
            {
                min_angle = angle;
                min_id = i;
            }
        }
        second_point = min_id;

        min_angle = 7;
        min_id = -1;
        for (int i = 0; i < count_; ++i)
        {
            if (first_point == i || second_point == i)
            {
                continue;
            }
            alias fp = first_point;
            alias sp = second_point;
            auto normal = vecMul(points[fp*3..fp*3+3], points[sp*3..sp*3+3], points[i*3..i*3+3]);
            double angle = angle([0, 0, 1], normal);
            if (min_angle > angle)
            {
                min_angle = angle;
                min_id = i;
            }
        }
        third_point = min_id;
        if (vecMul(
                    points[first_point*3..first_point*3+3], 
                    points[second_point*3..second_point*3+3], 
                    points[third_point*3..third_point*3+3])[1] > 0)
        {
            import std.algorithm:swap;
            swap(second_point, third_point);
        }
        float []new_normal = vecMul(
                    points[first_point*3..first_point*3+3], 
                    points[second_point*3..second_point*3+3], 
                    points[third_point*3..third_point*3+3]);
        verge ~= (Flatness(first_point, second_point, third_point, new_normal)); 
        edges ~= (Edge(first_point, second_point, 0));
        edges ~= (Edge(second_point, third_point, 0));
        edges ~= (Edge(third_point, first_point, 0));
    
    }
    int returnIsEdgeInHull(int a, int b) const
    {
        import std.range:enumerate;
        foreach(i,edge; enumerate(edges))
        {
            if ((edge.first == a && edge.second == b) ||
                (edge.first == b && edge.second == a))
            {
                return i.to!int;
            }
        }
    
        return -1;
    }
    void makeHull(){
        import std.container:SList;
        findFirstFlatness();
        SList!int stack = SList!int();
        static foreach(i; 0..3)
            stack.insertFront(i);
        while (!stack.empty)
        {
            float[] new_normal;
            stack.front.writeln;
            Edge e = edges[stack.front];
            stack.removeFront;
            if (e.is_close) 
            {
                continue;
            }
            int min_id = -1;
            float min_angle = 7;

            for (int i = 0; i < count_; ++i)
            {
                int another = verge[e.flatness].another(e.first, e.second);
                if (i != another && e.first != i && e.second != i) 
                {
                    float[] current_normal = vecMul( 
                            points[e.second*3..e.second*3+3], points[e.first*3..e.first*3+3], points[i*3..i*3+3]);
                    auto angle = angle(current_normal, verge[e.flatness].normal);

                    if (min_angle > angle)
                    {
                        min_angle = angle;
                        min_id = i;
                        new_normal = current_normal;
                    }
                }
            }
            if (min_id != -1) 
            {
                e.is_close = true; 
                int count_flatness = verge.length.to!int;
                int first_edge_in_hull = returnIsEdgeInHull(e.first, min_id); 
                int second_edge_in_hull = returnIsEdgeInHull(e.second, min_id);

                if (first_edge_in_hull == -1)
                {
                    edges ~= (Edge(e.first, min_id, count_flatness));
                    stack.insert(edges.length.to!int - 1);
                }
                if (second_edge_in_hull == -1)
                {
                    edges ~= (Edge(min_id, e.second, count_flatness));
                    stack.insert(edges.length.to!int - 1);
                }
                if (first_edge_in_hull != -1)
                {
                    edges[first_edge_in_hull].is_close = true;
                }
                if (second_edge_in_hull != -1)
                {
                    edges[second_edge_in_hull].is_close = true;
                }

                verge ~= (Flatness(e.first, e.second, min_id, new_normal));
            }
        }
    }
    this(float[] points){
        import std.conv:to;
        this.points = points;
        count_ = to!int(points.length/3);
        this.makeHull();
    }
}




import glamour.vao;
import glamour.vbo;
import glamour.shader;
class ShellRender{
    VAO vao;
    Buffer vertex,color,normal;
    Shader program;
    static immutable string shadersec = `
        #version 460 core
        vertex:
        attribute vec3 position;
        attribute vec3 vertexColor;
        attribute vec3 normal;
        
        // Output data ; will be interpolated for each fragment.
        out vec3 fragmentColor;
        out vec3 Normal;
        out vec3 FragPos;  
        // Values that stay constant for the whole mesh.
        uniform mat4 MVP;
        
        void main(){	
        	// Output position of the vertex, in clip space : MVP * position
        	gl_Position = MVP * vec4(position,1) ;
            FragPos = vec3(gl_Position);
            Normal = vec3(MVP*vec4(normal,1));
        
        	// The color of each vertex will be interpolated
        	// to produce the color of each fragment
            fragmentColor = vertexColor;
        }
        fragment:
    
        in vec3 fragmentColor;
        in vec3 Normal;
        in vec3 FragPos;
        // Ouput data
        out vec3 color;
        uniform vec3 lightPos;
        uniform mat4 MVP;
        
        void main(){
            vec3 norm = normalize(Normal);
            vec3 lightDir = normalize(FragPos- lightPos); 
            vec3 lightColor = vec3(1,1,1);
            float diff = max(dot(norm, lightDir), 0.1);
            vec3 diffuse = diff * lightColor;
            vec3 ambient = 0.45 * lightColor;
        
        	// Output color = color specified in the vertex shader, 
        	vec3 result = (ambient+diffuse)*fragmentColor;
            color = result;//vec4(result, 0.2f);// interpolated between all 3 surrounding vertices
        }
        `;
    GLuint vertexID;
    GLuint normalID;
    GLuint colorID;
    int len;
    this(float[] points){
        
        vao = new VAO();
        vao.bind();
        points = shell(points);
        float[] random_values(){
            import std.random:uniform;
            float[] t = [];
            t.length = points.length;
            foreach(i;0..t.length){
                t[i] = uniform(0f,1f);
            }
            return t;
        }
        len = points.length.to!int;
        vertex = new Buffer(points);
        normal = new Buffer(random_values);
        color = new Buffer(random_values);
        program = new Shader("shell",shadersec );
        vertexID  = program.get_attrib_location("position");
        normalID = program.get_attrib_location("normal");
        colorID = program.get_attrib_location("vertexColor");    
    }
    void draw()
    {
        import gl3n.linalg,std.math;
        glEnable(GL_DEPTH_TEST);
        static float i = 0;
        i+=0.005;
        vao.bind();
        glEnable(GL_DEPTH_TEST);
        mat4 projection = mat4.perspective(900,900,45,0.1f,100.0f);
        mat4 view;
        mat4 model = mat4(
           vec4(1,0,0,0),
           vec4(0,1,0,0),
           vec4(0,0,1,0),
           vec4(0,0,0,1),
           );
        view = mat4.look_at(
           vec3(20*sin(i) ,2+0.5*sin(i), 20*cos(i)),
           vec3(0,0,0),
           vec3(0,1,0));

        auto MVP = projection * view  * model;
        
        program.bind();
        glUniform3f(program.program,1,1,1);
        program.uniform("MVP", MVP);

        glEnableVertexAttribArray(vertexID);
        vertex.bind();
        glVertexAttribPointer(vertexID, 3, GL_FLOAT, GL_FALSE, 0, null);

        glEnableVertexAttribArray(colorID);
        color.bind();
        glVertexAttribPointer(colorID, 3, GL_FLOAT, GL_FALSE, 0, null);
        
        glEnableVertexAttribArray(normalID);
        normal.bind();
        glVertexAttribPointer(normalID, 3, GL_FLOAT, GL_FALSE, 0, null);
        static float j = 0.05;
        j+=0.05;
        glDrawArrays(GL_TRIANGLES, 0, j.to!int*3 % len + 3);

        glDisableVertexAttribArray(normalID);
        glDisableVertexAttribArray(colorID);
        glDisableVertexAttribArray(vertexID);
        vao.unbind();
    }
    void close()
    {
        // free resources
        vertex.remove();
        color.remove();
        normal.remove();
        program.remove();
        vao.remove();
    }

}

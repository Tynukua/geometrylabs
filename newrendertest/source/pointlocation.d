import derelict.opengl3.gl3;
import std.conv, std.stdio;

import glamour.vao: VAO;
import glamour.vbo: Buffer, ElementBuffer;
import glamour.shader: Shader;

class Polygon{
    static float[2] mouse = [0,0];
    ushort[] indices = [];
    float[] _points;
    VAO vao;
    Buffer points;
    ElementBuffer elements;
    GLuint positionID;
    Shader program;
    this(float[] _points){
        this._points = _points;
        vao = new VAO();
        vao.bind();
        points = new Buffer(_points);

        for (ushort i = 2;i<_points.length/2;i++){
            indices ~= [0, i-1, i].to!(ushort[]);
        }
        indices.writeln;
        elements = new ElementBuffer(indices);
        program = new Shader("Polygon", shader_source);
        program.bind();
        positionID = program.get_attrib_location("position");

    }
    void draw(){
        program.bind();
        program.uniform1f("colorCoef",(colored?0.5:1.5));
                
        points.bind();
        glEnableVertexAttribArray(positionID);
        glVertexAttribPointer(positionID, 2, GL_FLOAT, GL_FALSE, 0, null);
     
        elements.bind();
     
        glDrawElements(GL_TRIANGLES, indices.length.to!int, GL_UNSIGNED_SHORT, null);
     
        glDisableVertexAttribArray(positionID);
    }
    void close(){
        vao.remove();
        points.remove;
        program.remove;
        elements.remove;
    }
    bool colored(){
        float rotate(float[] A, float[]B, float[]C){
            return (B[0]-A[0])*(C[1]-B[1])-(B[1]-A[1])*(C[0]-B[0]);
        }
        bool intersect(T)(T A, T B, T C,T D){ 
            return rotate(A,B,C)*rotate(A,B,D)<=0 && rotate(C,D,A)*rotate(C,D,B)<0;
        }
        int count = 0;
        float[] D = [mouse[0],800f];

        alias P = _points;
        for(int i = 0;i<P.length/2-1;i++){
            auto j = i+1;
            if (intersect(P[i*2..i*2+2],P[j*2..j*2+2],mouse,D))
                count++;
        }
        if (intersect(P[0..2],P[$-2..$],mouse,D))
                count++;       
        if (count%2) return false;
        else return true;
        
        
        
        
        
        

        
        
    }
    static immutable string shader_source = `
        #version 460 core
        vertex:

        attribute vec3 position;
        out vec3 fragmentColor;

        void main(){
            gl_Position = vec4(0.2*position,1);
            fragmentColor = vec3(0.5*(position+1));
        }
        fragment:
        in vec3 fragmentColor;
        uniform float colorCoef;
        out vec3 color;
        void main(){
            color = fragmentColor * colorCoef;
        }


        `;
}

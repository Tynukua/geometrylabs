import std.stdio, std.math;
import std.conv:to;

import derelict.opengl3.gl3;

import glamour.vao: VAO;
import glamour.shader: Shader;
import glamour.vbo: Buffer, ElementBuffer;
import gl3n.linalg;

import objparser:ObjModel;

class Points
{
private:
    uint width_, height_;

    float[] vertices;
    ushort[] indices;

    VAO vao_;
    Shader program_;
    Buffer vbo_;
    Buffer normal;
    Buffer color;
    GLuint position_;
    GLuint normalID;
    GLuint colorID;
    static immutable string example_program_src_ = `
        #version 460 core
        vertex:
        attribute vec3 position;
        attribute vec3 vertexColor;
        attribute vec3 normal;
        
        // Output data ; will be interpolated for each fragment.
        out vec3 fragmentColor;
        out vec3 Normal;
        out vec3 FragPos;  
        // Values that stay constant for the whole mesh.
        uniform mat4 MVP;
        uniform vec3 offsets[100];
        
        void main(){	
        	// Output position of the vertex, in clip space : MVP * position
        	gl_Position = MVP * vec4(position*0.2+offsets[gl_InstanceID],1) ;
            FragPos = vec3(gl_Position);
            Normal = vec3(MVP*vec4(normal,1));
        
        	// The color of each vertex will be interpolated
        	// to produce the color of each fragment
            fragmentColor = vertexColor;
        }
        fragment:
    
        in vec3 fragmentColor;
        in vec3 Normal;
        in vec3 FragPos;
        // Ouput data
        out vec3 color;
        uniform vec3 lightPos;
        uniform mat4 MVP;
        
        void main(){
            vec3 norm = normalize(Normal);
            vec3 lightDir = normalize(FragPos- lightPos); 
            vec3 lightColor = vec3(1,1,1);
            float diff = max(dot(norm, lightDir), 0.1);
            vec3 diffuse = diff * lightColor;
            vec3 ambient = 0.45 * lightColor;
        
        	// Output color = color specified in the vertex shader, 
        	vec3 result = (ambient+diffuse)*fragmentColor;
            color = result;//vec4(result, 1.0f);// interpolated between all 3 surrounding vertices
        }
    `;
    float[] offsets;
public:
    this(float[] points = [0,0,0])
    {
        offsets = points;
        auto model = ObjModel("model.wf");

        vao_ = new VAO();
        vao_.bind();

        // Create VBO
        vbo_ = new Buffer(model.vertexs);
        normal = new Buffer(model.normals);
        float[] change_colors(){
            float[] r;
            foreach (v;model.vertexs){
                r ~= v*0.4+0.55;
            }
            return r;
        }
        color = new Buffer(change_colors());

        // Create IBO

        // Create program
        program_ = new Shader("example_program", example_program_src_);
        position_ = program_.get_attrib_location("position");
        normalID = program_.get_attrib_location("normal");
        colorID = program_.get_attrib_location("vertexColor");
    }

    void draw()
    {
        glEnable(GL_DEPTH_TEST);
        static float i = 0;
        i+=0.005;
        vao_.bind();
        glEnable(GL_DEPTH_TEST);
        mat4 projection = mat4.perspective(900,900,45,0.1f,100.0f);
        mat4 view;
        mat4 model = mat4(
           vec4(1,0,0,0),
           vec4(0,1,0,0),
           vec4(0,0,1,0),
           vec4(0,0,0,1),
           );
        view = mat4.look_at(
           vec3(20*sin(i) ,2+0.5*sin(i), 20*cos(i)),
           vec3(0,0,0),
           vec3(0,1,0));

        auto MVP = projection * view  * model;
        
        glClearColor(0.2,0.2,0.3,1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        program_.bind();
        glUniform3f(program_.program,1,1,1);
        program_.uniform("MVP", MVP);

        glEnableVertexAttribArray(position_);
        vbo_.bind();
        glVertexAttribPointer(position_, 3, GL_FLOAT, GL_FALSE, 0, null);

        glEnableVertexAttribArray(colorID);
        color.bind();
        glVertexAttribPointer(colorID, 3, GL_FLOAT, GL_FALSE, 0, null);
        
        glEnableVertexAttribArray(normalID);
        normal.bind();
        glVertexAttribPointer(normalID, 3, GL_FLOAT, GL_FALSE, 0, null);
        import std.algorithm:min;
        foreach(j;0..offsets.length/3/100+1){
            program_.uniform3fv("offsets",offsets[j*100..min((j+1)*100,$)]);
            glDrawArraysInstanced(GL_TRIANGLES,0,vbo_.length.to!int/3,
                    (-j*100 +min((j+1)*100,offsets.length)).to!int/3);
        }
        glDisableVertexAttribArray(normalID);
        glDisableVertexAttribArray(colorID);
        glDisableVertexAttribArray(position_);
        vao_.unbind();
    }

    void close()
    {
        // free resources
        vbo_.remove();
        color.remove();
        normal.remove();
        program_.remove();
        vao_.remove();
    }
}

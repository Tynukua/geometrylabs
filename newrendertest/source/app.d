module example;

import std.string;
import std.conv;
import std.stdio;

import derelict.opengl3.gl3;
import derelict.sdl2.sdl;

import points:Points;
import pointlocation:Polygon;
import poisson:poison;



class SDLApplication
{
    private SDL_Window* sdlwindow_;
    private OnDraw[] on_draw_;
    
    alias void delegate() OnDraw;
    
    @property
    onDraw(OnDraw on_draw) 
    { 
        assert(on_draw);
        on_draw_ ~= on_draw; 
    }

    this()
    {
        DerelictSDL2.load();
        DerelictGL3.load();

        if (SDL_Init(SDL_INIT_VIDEO) < 0) {
            throw new Exception("Failed to initialize SDL: " ~ to!string(SDL_GetError()));
        }

        // Set OpenGL version
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);

        // Set OpenGL attributes
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

        sdlwindow_ = SDL_CreateWindow("Glamour example application",
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            800,800, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

        if (!sdlwindow_)
            throw new Exception("Failed to create a SDL window: " ~ to!string(SDL_GetError()));

        SDL_GL_CreateContext(sdlwindow_);
        DerelictGL3.reload();
    }

    void run()
    {
        assert(on_draw_);
        auto run = true;
        while (run) {
            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                switch (event.type) {
                    case SDL_QUIT:
                        run = false;
                        break;
                    case SDL_MOUSEMOTION:
                        Polygon.mouse = [(event.motion.x-400)/400f*5,
                           -(event.motion.y-400)/400f*5];
                        break;

                    default:
                    break;
                }
            }
            foreach (drawcall; on_draw_)
                drawcall();

            SDL_GL_SwapWindow(sdlwindow_);
        }
    }
}

void main() {
    auto app = new SDLApplication();
    Polygon[] polygons = [
        new Polygon([-5,4, -4,4, -4,3, -5,3]),
        new Polygon([-1,1, 1,2, 1,-1,0,-2 ]),
        new Polygon([-2,-3, 1,-3, -2,-4, 1,-4]),
    ];
    scope(exit){
        foreach(p;polygons) p.close;
    }
    foreach(p;polygons)  app.onDraw = &p.draw;
    app.run();
}

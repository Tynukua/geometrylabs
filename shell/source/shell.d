import function_model:vecMul;
import std.conv:to;
import std.stdio;

float[] vecMul(float[] e, float[]a, float[] b){
    return vecMul([a[0]-e[0],a[1]-e[1],a[2]-e[2]],
            [b[0]-e[0],b[1]-e[1],b[2]-e[2]]);
    
}

float[] shell(float[] points){
    import core.exception;
    import std.stdio;
    ConvexHull h = new ConvexHull(points);
    float[] result;
    foreach(Flatness flat;h.verge){
        result~= h.points[flat.first*3..flat.first*3+3];
        result~= h.points[flat.second*3..flat.second*3+3];
        result~= h.points[flat.third*3..flat.third*3+3];
    }
    return result;

}

float angle(float[] a, float[]b){
    import std.math: acos;
    auto la = len(a), lb = len(b);
    auto prod = a[0]*b[0] +a[1]*b[1] +a[2]*b[2];
    if (prod ==0) return 0;
    return acos(prod/(la*lb));


}
float len(float[] v){
    import std.math:sqrt;
    float sum = 0;
    foreach(float _;v){
        sum+=_*_;
    }
    return sqrt(sum);
}

struct Edge
{
    int first;
    int second;
    int flatness; //номер грани
    bool is_close = false; // открыто ли наше ребро, можно ли добавить еще грань?
    this(int first, int second, int flatness = -1, float[] normal = [0,0,0]) {
        this.first=first;
        this.second = second;
        this.flatness = flatness;
    }
}
struct Flatness 
{
    int first;
    int second;
    int third;
    float[] normal;

    this(int first, int second, int third, float[] normal) {
        this.first=first;
        this.second = second;
        this.third = third;
        this.normal = normal;
    }
    int another(int one, int two){
    return 0;}
}

class ConvexHull
{

    float[] points = []; // Точки изначального множества
    Flatness[] verge = []; // Грани
    Edge[] edges = []; // Ребра

    int count_; // Количество точек изначального множества

    int findMinZ() const{
        int min_id = 0;
        for (int i = 1; i < count_; ++i)
        {
            if(points[i*3+1] <points[min_id*3+1])
                min_id = i;
            else if(points[i*3+1] <points[min_id*3+1]){
                if (points[i*3]  <points[min_id*3] || 
                        points[i*3+2] < points[min_id*3+2])
                    min_id = i;
            }
        }

        return min_id;
    }
    void findFirstFlatness(){
        int first_point, second_point, third_point; // наши первые три точки
        first_point = findMinZ();
        double min_angle = 7;
        int min_id = -1;
        for (int i = 0; i < count_; ++i)
        {
            if (first_point == i) 
            {
                continue;
            }
            float[] start = points[first_point*3..first_point*3+3];
            float[] next = points[i*3..i*3+3];
            double angle = angle(
                    [start[0]- next[0],start[1]- next[1],start[2]- next[2]],
                    [0,next[1]-start[1],0]);
            if (min_angle > angle)
            {
                min_angle = angle;
                min_id = i;
            }
        }
        second_point = min_id;

        min_angle = 7;
        min_id = -1;
        for (int i = 0; i < count_; ++i)
        {
            if (first_point == i || second_point == i)
            {
                continue;
            }
            alias fp = first_point;
            alias sp = second_point;
            auto normal = vecMul(points[fp*3..fp*3+3], points[sp*3..sp*3+3], points[i*3..i*3+3]);
            double angle = angle([0, 0, 1], normal);
            if (min_angle > angle)
            {
                min_angle = angle;
                min_id = i;
            }
        }
        third_point = min_id;
        if (vecMul(
                    points[first_point*3..first_point*3+3], 
                    points[second_point*3..second_point*3+3], 
                    points[third_point*3..third_point*3+3])[1] > 0)
        {
            import std.algorithm:swap;
            swap(second_point, third_point);
        }
        float []new_normal = vecMul(
                    points[first_point*3..first_point*3+3], 
                    points[second_point*3..second_point*3+3], 
                    points[third_point*3..third_point*3+3]);
        verge ~= (Flatness(first_point, second_point, third_point, new_normal)); // нулевая грань
        edges ~= (Edge(first_point, second_point, 0));
        edges ~= (Edge(second_point, third_point, 0));
        edges ~= (Edge(third_point, first_point, 0));
    
    }
    int returnIsEdgeInHull(int a, int b) const
    {
        import std.range:enumerate;
        foreach(i,edge; enumerate(edges))
        {
            if ((edge.first == a && edge.second == b) ||
                (edge.first == b && edge.second == a))
            {
                return i.to!int;
            }
        }
    
        return -1;
    }
    void makeHull(){
        import std.container:SList;
        findFirstFlatness();
        SList!int stack = SList!int();
        static foreach(i; 0..3)
            stack.insertFront(i);
        while (!stack.empty)
        {
            float[] new_normal;
            stack.front.writeln;
            Edge e = edges[stack.front];// берем верхнее ребро, рассматриваем его
            stack.removeFront;
            if (e.is_close) // если в ребро уже нельзя добавлять грани, мы этого не делаем
            {
                continue;
            }
            int min_id = -1;
            float min_angle = 7;

            for (int i = 0; i < count_; ++i)
            {
                int another = verge[e.flatness].another(e.first, e.second);
                if (i != another && e.first != i && e.second != i) // проверка, что точка не из той же грани
                {
                    // нахождение нормали к плоскости, образуемой ребром и i-ой точкой
                    float[] current_normal = vecMul(points[e.second*3..e.second*3+3], points[e.first*3..e.first*3+3], points[i*3..i*3+3]);
                    auto angle = angle(current_normal, verge[e.flatness].normal);

                    if (min_angle > angle)
                    {
                        min_angle = angle;
                        min_id = i;
                        new_normal = current_normal;
                    }
                }
            }
            if (min_id != -1) // защита от дурака - если в оболочке меньше 4х вершин
            {
                e.is_close = true; // раз мы рассмоттрели это ребро, значит добавили уже вторую грань к этому ребру
                int count_flatness = verge.length.to!int;// номер нашей грани
                int first_edge_in_hull = returnIsEdgeInHull(e.first, min_id); // возвращает -1, если ребра еще нет
                int second_edge_in_hull = returnIsEdgeInHull(e.second, min_id);

                if (first_edge_in_hull == -1)
                {
                    edges ~= (Edge(e.first, min_id, count_flatness));
                    stack.insert(edges.length.to!int - 1);
                }
                if (second_edge_in_hull == -1)
                {
                    edges ~= (Edge(min_id, e.second, count_flatness));
                    stack.insert(edges.length.to!int - 1);
                }
                if (first_edge_in_hull != -1)
                {
                    edges[first_edge_in_hull].is_close = true;
                }
                if (second_edge_in_hull != -1)
                {
                    edges[second_edge_in_hull].is_close = true;
                }

                verge ~= (Flatness(e.first, e.second, min_id, new_normal));
            }
        }
    }
    this(float[] points){
        import std.conv:to;
        this.points = points;
        count_ = to!int(points.length/3);
        this.makeHull();
    }
}


import buffer:Buffer;
import shader:ShaderProgram;
class ShellRenderer{
    Buffer vertexs;
    ShaderProgram program;

    this(float[] points){
    
    }
    

}



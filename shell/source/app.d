import std.stdio;
import core.math;

import glfw3.api:glfwSwapBuffers,glfwInit;
import bindbc.opengl;
import glm = gl3n.linalg;

import shader: ShaderProgram ;
import window: Window;
import buffer: Buffer;
import std.random: uniform;
import figures: colorer;
import objparser: ObjModel;
import function_model: Function;
import poisson:poison, FLAT,RANDOM;




int main()
{
   glfwInit();
   auto w = new Window(900,900, "Polygon");
   auto program = ShaderProgram( 
           [
            "vertex_shader.glsl": GL_VERTEX_SHADER, 
            "fragment_shader.glsl": GL_FRAGMENT_SHADER] );

   ///MATRIX
   glm.mat4 projection = glm.mat4.perspective(900,900,45,0.1f,100.0f);
   glm.mat4 view;
   glm.mat4 model = glm.mat4(
           glm.vec4(1,0,0,0),
           glm.vec4(0,1,0,0),
           glm.vec4(0,0,1,0),
           glm.vec4(0,0,0,1),
           );
   GLuint MatrixID = glGetUniformLocation(program.programID, "MVP");
   //MATRIXEND
   GLuint lightPosID = glGetUniformLocation(program.programID, "lightPos");
   GLuint offsetsID= glGetUniformLocation(program.programID, "offsets");
   auto m = ObjModel("./model.wf");
   auto buffer = new Buffer(m.vertexs);
   auto colors = new Buffer(colorer([0.5,0.5,0.5],[0.2,0.2,0.2],buffer.data));
   auto normalbuf= new Buffer(m.normals); 
   glEnable(GL_DEPTH_TEST);
   float i = 0;
   glm.mat4 MVP;
   static if(!RANDOM)
       float[] offsetsfull = poison(40,40,4,60);
   else{
       float[] offsetsfull;
           static if(FLAT)
               enum _coef = 1;
           else
               enum _coef = 3;
       foreach(int _; 0..400*_coef){
           offsetsfull~= uniform(-20,20f);
           offsetsfull~= uniform(-20,20f);
           static if(!FLAT)
               offsetsfull~= uniform(-20,20f);
           else offsetsfull~= 0;
       }
   }
   offsetsfull.length.writeln;
   
   while(!w.shouldClose ){
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        import std.conv:to;
        buffer.enable();
        colors.enable();
        normalbuf.enable();
        program.use();
        glUniform3f(lightPosID, 1,1,1);
        /// 1ST Object
        view = glm.mat4.look_at(
           glm.vec3(60*sin(i) ,2+0.5*sin(i), 60*cos(i)),
           glm.vec3(0,0,0),
           glm.vec3(0,1,0));

        MVP = projection * view  * model;
        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
        for(int j= 0; j< offsets.length/300;j++){
        glUniform3fv(offsetsID,
                100,
                offsets[j*300..(j+1)*300].ptr);
        glDrawArraysInstanced(
                GL_TRIANGLES, 0, cast(int)(buffer.data.length/3 ),
                cast(int)(100)); 
        }
        if (offsets.length%300){

            auto lj =  offsets.length%300;
       glUniform3fv(offsetsID,cast(int)lj/3,offsets[$-lj..$].ptr);
       glDrawArraysInstanced(
                GL_TRIANGLES, 0, cast(int)(buffer.data.length/3 ),
                cast(int)(lj/3)); 
        }


        i+=0.005;
        
        normalbuf.disable();
        buffer.disable();
        colors.disable();

        

        w.swapBuffers();
        w.pollEvents();
   }
   return 0;   
}

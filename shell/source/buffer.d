module buffer;

import bindbc.opengl;

class Buffer{
    private static int lastBufferNumber = 0;
    float[] data;
    int bufferNumber;
    GLuint bufferId;
    
    this(float[] bufferData){
        this.data = bufferData;
        bufferNumber = lastBufferNumber++;
        GLuint VertexArrayID;
        glGenVertexArrays(1, &VertexArrayID);
        glBindVertexArray(VertexArrayID); 
        // Generate 1 buffer, put the resulting identifier in vertexbuffer
        // The following commands will talk about our 'vertexbuffer' buffer
        glGenBuffers(1, &bufferId);
        glBindBuffer(GL_ARRAY_BUFFER, bufferId);
        
    }
    void enable(){
        glEnableVertexAttribArray(bufferNumber);
		glBindBuffer(GL_ARRAY_BUFFER, bufferId);
        glBufferData(GL_ARRAY_BUFFER,
                data.length*float.sizeof,
                data.ptr,GL_STATIC_DRAW);
        glVertexAttribPointer(
                bufferNumber,
                3,
                GL_FLOAT,
                GL_FALSE,
                0,
                null);
    }
    void disable(){
        glDisableVertexAttribArray(bufferNumber);
    }

    ~this(){
        glDeleteBuffers(1, &bufferId);
    }
}



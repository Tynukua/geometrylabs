import std.math;
import std.random;
import std.stdio:writeln;

enum FLAT = 0;
enum RANDOM = 1;

struct Point(T){
    T x,y,z;
    this(T x, T y, T z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
alias FloatPoint= Point!float;
alias IntPoint = Point!int;

struct Grid{
    FloatPoint[IntPoint] grid;
    float cellsize;
    this(float _cellsize){cellsize =_cellsize;}
    void add(FloatPoint p){
        grid[this.to(p)] = p;
    }

    IntPoint to(FloatPoint p){
        return IntPoint(
                cast(int) (p.x/cellsize),
                cast(int) (p.y/cellsize), 
                cast(int) (p.z/cellsize));
    
    }
    FloatPoint get(IntPoint p){
        return grid[p];
    }
    
    bool canPlaced(FloatPoint p, float r){
        auto ip = to(p);
        for (int i = -1;i<2;i+=2){
            for (int j = -1;j<2;j+=2){
                for (int k = -1;k<2;k+=2){
                    auto cp = ip;
                    cp.x+=i,cp.y+=j,cp.z+=k;
                    if(cp in grid){
                        auto cpc = grid[cp];
                        static if(FLAT){
               if(sqrt(pow(cp.x-cpc.x,2)+pow(cp.y-cpc.y,2))<r) return false;}
                        else{
               if(sqrt(pow(cp.x-cpc.x,2)+pow(cp.y-cpc.y,2)+pow(cp.z-cpc.z,2))<r) 
                            return false;}
                    }
                
                }
            }
        }
        return true;
    }

}
float rnd_excluded(float a, float b, float c, float d){
    if(uniform!"[]"(0,1)){
        return uniform!"[]"(a,b);
    }else{
       return uniform!"[]"(c,d);
    }

}

float[] poison(float x,float y,float r = 2 , int k = 50){
    float z = x;
    float[] result;
    float[] q;
    auto cellsize = r/sqrt(2f);
    auto grid = Grid(cellsize);
    static if(FLAT){
    auto p = FloatPoint(
            uniform!"[]"(x/4, x/2), uniform!"[]"(y/4,y/2),uniform!"[]"(x/4,z/2));
    result ~= [p.x-x/2, p.y -y/2, p.z -z/2];}
    else{
    auto p = FloatPoint(
            uniform!"[]"(x/4, x/2), uniform!"[]"(y/4,y/2),0);
    result ~= [p.x-x/2, p.y -y/2,0];}
    
    
    q ~= [p.x, p.y, p.z];
    grid.add(p);
    while (q.length){
        auto s = q[$-3..$];
        q.length -= 3;
        for(int _ = 0; _<k; _++){
            alias cl = cellsize;
            p = FloatPoint(s[0],s[1],s[2]);
            auto intp = grid.to(p);
            static if(FLAT){
            auto newp = FloatPoint(
                rnd_excluded((intp.x-1)*cl, (intp.x)*cl,(intp.x+1)*cl, (intp.x+2)*cl),
                rnd_excluded((intp.y-1)*cl, (intp.y)*cl,(intp.y+1)*cl, (intp.y+2)*cl),
                0
                );}
            else{
            auto newp = FloatPoint(
                rnd_excluded((intp.x-1)*cl, (intp.x)*cl,(intp.x+1)*cl, (intp.x+2)*cl),
                rnd_excluded((intp.y-1)*cl, (intp.y)*cl,(intp.y+1)*cl, (intp.y+2)*cl),
                rnd_excluded((intp.z-1)*cl, (intp.z)*cl,(intp.z+1)*cl, (intp.z+2)*cl)
                );}
            if (!grid.canPlaced(newp,r)) continue;
            if (grid.to(newp) in grid.grid)continue;
            if(newp.x<0  || newp.x >x)continue;
            if(newp.z<0  || newp.z >x)continue;
            if(newp.y<0  || newp.y> y)continue;
            grid.add(newp);
            q ~= [newp.x, newp.y, newp.z];
            static if (FLAT)
            result ~= [newp.x-x/2,newp.y-y/2,0];
            else
            result ~= [newp.x-x/2,newp.y-y/2 , newp.z-x/2];
        }
    }
    return result;
}



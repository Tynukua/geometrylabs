import std.math;


float[] vecMul(float[]a, float[]b){
    return [
        a[1]*b[2] - a[2]*b[1],
        a[2]*b[0] - a[0]*b[2],
        a[0]*b[1] - a[1]*b[0]
    ];
}

struct Function{
    float x0, x1;
    float z0, z1;
    float step = 0.01;
    float[] vertexs;
    float[] normals;
    float delegate(float, float) f;

    this(   float delegate(float, float) f, 
            float xAcesStart, 
            float xAcesEnd,
            float yAcesStart, 
            float yAcesEnd ){
        this.f = f;
        x0 = xAcesStart;
        x1 = xAcesEnd;

        z0 = yAcesStart;
        z1 = yAcesEnd;
        generate();
    }
    private void generate(){
        vertexs = [];
        normals = [];
        for (float i = x0;i<x1;i+=step){
            for(float j=z0;j<z1;j+=step)
                gensqr(i,j);
        }
        
    }
    private void gensqr(float x, float z){
        // S1t POLYGON
        auto l = vertexs.length;
        vertexs ~= [x,f(x,z),z];
        vertexs ~= [x,f(x,z+step),z+step];
        vertexs ~= [x+step,f(x+step,z),z];
        normals ~= vecMul(
                [0,f(x,z+step)-f(x,z),step],
                [step,f(x+step,z)-f(x,z),step]); 
        normals ~= normals[l..l+3];
        normals ~= normals[l..l+3];

        //2nd POLYGON
        l = vertexs.length;
        vertexs ~= [x,f(x,z+step),z+step];
        vertexs ~= [x+step,f(x+step,z),z];
        vertexs ~= [x+step,f(x+step,z+step),z+step];
        normals ~= vecMul(
                [step,f(x+step,z+step)-f(x,z+step),0],
                [0,f(x+step,z+step)-f(x+step,z),step]); 
        normals ~= normals[l..l+3];
        normals ~= normals[l..l+3];
        
    }
    

}


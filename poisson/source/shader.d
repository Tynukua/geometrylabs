import std.stdio;
import bindbc.opengl;
/**
 * 
 */

struct ShaderProgram{
    GLuint programID;
    GLenum[string] pathes;
    this(GLenum[string] shaderFilePathes ){
        pathes = shaderFilePathes;
        programID = glCreateProgram();
        compile();
    }
    private void compile(){
        GLuint[]shaderIDs = new GLuint[pathes.keys.length];
        int i = 0;
        foreach (string path; pathes.keys ){
            auto shaderID = glCreateShader(pathes[path]);
            char * code;
            auto file = File(path, "r");
            if(file.isOpen()){
                code = cast(char*)file.rawRead(new char[file.size+1]);
                code[file.size] = 0;
                file.close();
            }
            else{
		        writefln("Impossible to open %s. 
                    Are you in the right directory ? 
                    Don't forget to read the FAQ !\n", path);
            }
            // COMPILING
            writefln("Compiling shader: %s", path);
            glShaderSource(shaderID,1,&code,null);
	        glCompileShader(shaderID);
            int result, logLen;
	        glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
        	glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &logLen);
        	if (logLen> 0){
		        char[] shaderErrorMessage = new char[logLen+1];
        		glGetShaderInfoLog(shaderID, logLen, null, shaderErrorMessage.ptr);
		        writeln(shaderErrorMessage);
            }
           
           
            shaderIDs[i++] = shaderID;
        }
        // LINKING
        writeln("Linking program");
        foreach (GLuint shaderID; shaderIDs){
            glAttachShader(this.programID, shaderID);
        }
        glLinkProgram(this.programID);
        int result, logLen;
	    glGetProgramiv(this.programID, GL_LINK_STATUS, &result);
        glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &logLen);
        if ( logLen> 0 ){
		    char[] shaderErrorMessage = new char[logLen+1];
        	glGetShaderInfoLog(programID, logLen, null, shaderErrorMessage.ptr);
		    writeln(shaderErrorMessage);
        }
        foreach (GLuint shaderID; shaderIDs){
            glDetachShader(this.programID, shaderID);
            glDeleteShader(shaderID);
        }
    
    }
    /**
        use compiled shaderprogram
     */
    void use(){
        return glUseProgram(this.programID);
    }

}

#version 460 core

in vec3 fragmentColor;
in vec3 Normal;
in vec3 FragPos;
// Ouput data
out vec3 color;
uniform vec3 lightPos;
uniform mat4 MVP;

void main(){
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(FragPos- lightPos); 
    vec3 lightColor = vec3(1,1,1);
    float diff = max(dot(norm, lightDir), 0.1);
    vec3 diffuse = diff * lightColor;
    vec3 ambient = 0.45 * lightColor;

	// Output color = color specified in the vertex shader, 
	vec3 result = (ambient+diffuse)*fragmentColor;
    color = result;//vec4(result, 1.0f);// interpolated between all 3 surrounding vertices

}

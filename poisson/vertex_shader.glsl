#version 460 core
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 vertexColor;
layout(location = 2) in vec3 normal;

// Output data ; will be interpolated for each fragment.
out vec3 fragmentColor;
out vec3 Normal;
out vec3 FragPos;  
// Values that stay constant for the whole mesh.
uniform mat4 MVP;
uniform vec3 offsets[100];

void main(){	
	// Output position of the vertex, in clip space : MVP * position
	gl_Position = vec4(position*0.2+offsets[gl_InstanceID],1) * MVP ;
    FragPos = vec3(gl_Position);
    Normal = vec3(vec4(normal,1)*MVP);

	// The color of each vertex will be interpolated
	// to produce the color of each fragment
	fragmentColor = vertexColor;
}

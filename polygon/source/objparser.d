
import std.stdio;

//void readWord(FILE*f, char[] buf){
//    char c;
//    int i =0;
//    while( (c=cast(char)getc(f))!=' '){
//        buf[i++] =c;
//    }
//    buf[i]= 0;
//}

struct ObjModel{
    float[]vertexs = [];
    float[]normals = [];
    this(string path){
        FILE * f = fopen(cast(char*)path, "r");
        float[] vertexList = [],normalList=[];
        do {

            float x,y,z;
            int n,v;
            char[32]buf;
            fscanf(f,"%s", buf.ptr);
            if(buf[0] == 'v'){
                fscanf(f, "%f%f%f", &x,&y,&z);
                if(buf[1] == 'n'){
                    normalList~= [x,y,z];
                }
                else if(buf[1]==0){
                    vertexList~=[x,y,z];
                }

            }
            else if(buf[0] == 'f'){
                int void_;
                for(int i = 0;i<3;i++){
                    fscanf(f,"%d/%d/%d", &v,&void_, &n);
                    v-=1;
                    n-=1;
                    vertexs ~= vertexList[v*3..v*3+3];
                    normals ~= normalList[n*3..n*3+3];

                }
            }
            while(getc(f)!='\n' && !feof(f)){}
            
        }while(!feof(f));
        fclose(f);
    }
}

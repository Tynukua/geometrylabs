import std.stdio;
import core.math;

import glfw3.api:glfwSwapBuffers,glfwInit;
import bindbc.opengl;
import glm = gl3n.linalg;

import shader: ShaderProgram ;
import window: Window;
import buffer: Buffer;
import figures: colorer;
import objparser: ObjModel;
import function_model: Function;

int main()
{
   glfwInit();
   auto w = new Window(900,900, "Polygon");
   auto program = ShaderProgram( 
           [
            "vertex_shader.glsl": GL_VERTEX_SHADER, 
            "fragment_shader.glsl": GL_FRAGMENT_SHADER] );
    
   ///MATRIX
   glm.mat4 projection = glm.mat4.perspective(900,900,45,0.1f,100.0f);
   glm.mat4 view;
   glm.mat4 model = glm.mat4(
           glm.vec4(1,0,0,0),
           glm.vec4(0,1,0,0),
           glm.vec4(0,0,1,0),
           glm.vec4(0,0,0,1),
           );
   GLuint MatrixID = glGetUniformLocation(program.programID, "MVP");
   //MATRIXEND
   GLuint lightPosID = glGetUniformLocation(program.programID, "lightPos");
   GLuint iId = glGetUniformLocation(program.programID, "i");
   Function m = Function((
           (float x, float y)=> 0.1*cos(0.1+sqrt(400*x*x+400*y*y))),-1,1,-1,1);
   auto buffer = new Buffer(m.vertexs);
   auto colors = new Buffer(colorer([0.5,0.5,0.5],[0.2,0.2,0.2],buffer.data));
   auto normalbuf= new Buffer(m.normals); 
   glEnable(GL_DEPTH_TEST);
   float i = 0;
   while(!w.shouldClose ){
            glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        pure float f(float x, float y){
            return           0.1*cos(i*10+sqrt(400*x*x+400*y*y));
        }
        //m = Function((float x, float y)=>f(x,y) ,-1,1,-1,1);
        //buffer.data = m.vertexs;
        //colors.data = colorer([0.5,0.5,0.5],[0.3,0.3,0.3],buffer.data);
        normalbuf.data = m.normals;
        buffer.enable();
        colors.enable();
        normalbuf.enable();
        program.use();
        view = glm.mat4.look_at(
           glm.vec3(0, 2, 4),
           glm.vec3(0, 0, 0),
           glm.vec3(0, 1, 0));
        
        glm.mat4 MVP = projection * view  * model;
        glUniform1f(iId,i*10);
        glUniform3f(lightPosID, 1,1,1);
        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
        glDrawArrays(GL_TRIANGLES, 0, cast(int)(buffer.data.length/3 )); 
        i+=0.01;
        
        normalbuf.disable();
        buffer.disable();
        colors.disable();
        w.swapBuffers();
        w.pollEvents();

        
   }
   return 0;   
}

module window;

import stdio = std.stdio;
import glfw = glfw3.api;
import bindbc.opengl;

/**
*/
class Window{
    /**
    */
    int width;
        /**
    */
    int height;    /**
    */
    glfw.GLFWwindow * window;
        /**
    */
    this(int w, int h, string title){
        width = w;
        height = h;
        glfw.glfwWindowHint(glfw.GLFW_SAMPLES, 4);
        glfw.glfwWindowHint(glfw.GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfw.glfwWindowHint(glfw.GLFW_CONTEXT_VERSION_MINOR, 6);
        glfw.glfwWindowHint(glfw.GLFW_OPENGL_PROFILE, 

                glfw.GLFW_OPENGL_CORE_PROFILE);
       // glfw.glfwWindowHint(glfw.GLFW_DOUBLEBUFFER, GL_FALSE);
 //       glfw.glfwWindowHint(glfw.GLFW_RESIZABLE, GL_FALSE);
        window = glfw.glfwCreateWindow(
           width, height, cast(char*)title,
           cast(glfw.GLFWmonitor*)0, cast(glfw.GLFWwindow*) 0);
        glfw.glfwMakeContextCurrent(window);
        GLSupport retVal = loadOpenGL();
        stdio.writeln("Version:", retVal);
        if (retVal != GLSupport.gl46)
            throw new Exception("OpenGL version");
    }
    ~this(){
        glfw.glfwTerminate();
        stdio.writeln("window closed");
    }
    /**
    */
    bool shouldClose(){
        return cast(bool) glfw.glfwWindowShouldClose(this.window);
    }
    /**
    */
    void pollEvents(){
        glfw.glfwPollEvents();
    }
    /**
    */
    void swapBuffers(){
        glfw.glfwSwapBuffers(window);
    }

}


float [] colorer(float[3]b, float[3]e, float[] vector){
    auto len = vector.length;
    auto r = new float[len];
    for(int i = 0;i<len/3;i++){
        r[i*3..i*3+3] =[
            b[0]+ e[0]*vector[i*3+0], 
            b[1]+ e[1]*vector[i*3+1],
            b[2]+ e[2]*vector[i*3+2]];
 
    }
    return r;
}


cmake_minimum_required(VERSION 3.5)
project (intalg)
set (CMAKE_C_STANDARD 99)
set(CMAKE_C_COMPILER "/usr/bin/gcc-10")
set(CMAKE_CXX_COMPILER "/usr/bin/g++-9")
add_executable(intalg
    src/main.c 
    src/drawcall.c 
    src/drawcall.h
    src/mainfuncs.c
    src/mainfuncs.h
    )
target_link_libraries(intalg m)
target_link_libraries(intalg ncurses)


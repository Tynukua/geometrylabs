#ifndef DRAWCALL_H
#define DRAWCALL_H
#include <curses.h>
#include <math.h>



void drawcall(float * buf, float max);

float drawElipse(float * buf,int points[4]);
float drawBresenhamLine(float* buf,int points[4]);

float drawLineAA(float* buf, int points[4]);

#endif

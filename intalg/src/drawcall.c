#include <stdio.h>
#include "drawcall.h"


#define ABS(x) (x>=0?x:-x)

void plot(float *buf, int x, int y, float value){
    int ii = x + y*COLS;
//    if(!(x< COLS || y < LINES)) return;
    if(x>=COLS && y>=LINES) exit(2);
    buf[ii] = value;
    
}
int sign(int a){
    if (a){
        return (a>0?1:-1);
    }
    else return 0;
}

float A = 1, B = 1, C =1;

void drawcall(float * buf, float max){
    for (int  i = 0;i<LINES;i++){
        move(i, 0);
        for (int j = 0;j<COLS;j++){
            int bufi = (i*COLS+j);
            int N = buf[bufi]/(max/12);
            if (buf[bufi]==0){
                addch(' ');
            }
            else{
                int N = buf[bufi]/(max/11);
             
                addch(".,-~:;!*=#$@"[(N>0?N:0)%12]);
            }
            buf[bufi]=0;
        }
    }
}

float drawBresenhamLine(float* buf,int points[4]){
    int x0 = points[0];
    int x1 = points[2];
    int y0 = points[1];
    int y1 = points[3];
    float max = 1;
    int A, B, sign;
    A = y1 - y0;
    B = x0 - x1;
    if (ABS(A) > ABS(B)) sign = 1;
    else sign = -1;
    int signa, signb;
    if (A < 0) signa = -1;
    else signa = 1;
    if (B < 0) signb = -1;
    else signb = 1;
    int f = 0;
    int i = y0*COLS + x0;
    if(i>=0 && i<COLS*LINES)  buf[i] = max;
    int x = x0, y = y0;
    if (sign == -1) {
        while (x != x1 || y != y1)  {
            f += A*signa;
            if (f > 0) {
                f -= B*signb;
                y += signa;
            }
            x -= signb;
            int i = y*COLS + x;
            if(i>=0 && i<COLS*LINES)  buf[i] = max;
        } 
    }
    else {
        while (x != x1 || y != y1){
        f += B*signb;
        if (f > 0) {
            f -= A*signa;
            x -= signb;
        }
        y += signa;
        int i = y*COLS + x;
        if(i>=0 && i<COLS*LINES)  buf[i] = max;
    } 
}

    return max;
}

float drawLineAA(float* buf, int points[4]) 
{
    int x0 = points[0];
    int x1 = points[2];
    int y0 = points[1];
    int y1 = points[3];

    
    int max = 12;

    int dx = ABS(x1-x0), sx = x0 < x1 ? 1 : -1;
    int dy = ABS(y1-y0), sy = y0 < y1 ? 1 : -1;
    int x2, e2, err = dx-dy;

    int ed = dx+dy == 0 ? 1 : sqrt((float)dx*dx+(float)dy*dy);
    while(1){

         plot(buf,x0,y0, max - max*ABS(err-dx+dy)/ed);
         e2 = err; x2 = x0;
         if (2*e2 >= -dx) {

             if (x0 == x1) break;
             if (e2+dy < ed) plot(buf,x0,(y0+sy) , max- max*(e2+dy)/ed);
             err -= dy; x0 += sx;
         }
         if (2*e2 <= dy) {

             if (y0 == y1) break;
             if (dx-e2 < ed) plot(buf,x2+sx,y0, max- max*(dx-e2)/ed);
             err     += dx; y0 += sy;
         }
   }

    return max;
}

#define pixel4(b,x,y,_y,_x){ \
    plot(b,x+_x,y+_y,10); \
    plot(b,x+_x,y-_y,10); \
    plot(b,x-_x,y+_y,10); \
    plot(b,x-_x,y-_y,10); \
}

float drawElipse(float * buf,int points[4])
{
    int x = points[0];
    int y = points[1];
    int a = points[2];
    int b = ABS(points[3]);
    int _x = 0; // Компонента x
    int _y = b; // Компонента y
    int a_sqr = a * a; // a^2, a - 
    int b_sqr = b * b; // b^2, b -
    int delta = 4 * b_sqr*((_x + 1)*(_x + 1)) +\
        a_sqr*((2*_y - 1) * (2*_y - 1)) - 4*a_sqr * b_sqr; 
    while (a_sqr * (2*_y - 1) > 2*b_sqr * (_x + 1)) {
        pixel4(buf, x, y, _x, _y);
        if (delta < 0) {
            _x++;
            delta += 4 * b_sqr * (2 * _x + 3);
        }
        else {
            _x++;
            delta = delta - 8 * a_sqr * (_y - 1) + 4 * b_sqr * (2 * _x + 3);
            _y--;
        }
    }
    delta = b_sqr * ((2 * _x + 1) * (2 * _x + 1)) + \
        4 * a_sqr * ((_y + 1) * (_y + 1)) - 4 * a_sqr * b_sqr; 
    while (_y + 1 != 0) {
        pixel4(buf,x, y, _x, _y);
        if (delta < 0){ 
            _y--;
            delta += 4 * a_sqr * (2 * _y + 3);
        }
        else {
            _y--;
            delta = delta - 8 * b_sqr * (_x + 1) + 4 * a_sqr * (2 * _y + 3);
            _x++;
        }
    }
    return 10;
}

#include <stdlib.h>
#include <unistd.h>

#include "drawcall.h"
#include "mainfuncs.h"

int line(int argc, char ** argv, float(*func)(float*, int*)){
    
    float*  f = NULL;
    int fx = -1, fy = -1;
    int p[] = {2,3,9,10};
    while(1){
        //resize buf
        if(COLS != fx || LINES != fy){
            if(f) free(f);
            f = malloc(sizeof(float)*COLS*LINES);
            fx = COLS;
            fy = LINES;
        }
    
        //drawing
        
       
        drawcall(f, func(f, p));
        mvprintw(0,0,"[%d; %d] [%d; %d]",p[0],p[1],COLS,LINES);
        refresh();
        usleep(50000);
        char c = getch();
        switch (c)
        {
            case 'w':
                p[3]--;
                break;
            case 's':
                p[3]++;
                break;
            case 'a':
                p[2]--;
                break;
            case 'd':
                p[2]++;
                break;        
            case 'i':
                p[1]--;
                break;
            case 'k':
                p[1]++;
                break;
            case 'j':
                p[0]--;
                break;
            case 'l':
                p[0]++;
                break;
            
            default:
                break;
        }
    }
    return 0;
}

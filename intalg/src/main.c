#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "drawcall.h"
#include "mainfuncs.h"
extern float A,B,C;

int main(int argc, char *argv[]){
    initscr();
    noecho();
    curs_set(0);
    if(argc>1){
        if(!strcmp(argv[1], "Bresenham"))
            line(argc-1,argv+1,drawBresenhamLine);
        else if(!strcmp(argv[1], "AA"))
            line(argc-1,argv+1,drawLineAA);
        else if(!strcmp(argv[1], "elipse"))
            line(argc-1,argv+1,drawElipse);
        else {
            endwin();
            perror("No such procedure");
            exit(1);
        }
    }else{
            endwin();
            perror("???");
            exit(1);
    }
    endwin();
    return 0;
}

